package ej2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Escribe un programa que establezca un pequeño diálogo entre un programa
 * servidor y sus clientes, que intercambiarán cadenas de información. El
 * programa cliente se conecta al servidor y, una vez conectado, lee una cadena
 * del servidor y la escribe en la pantalla. Cada vez que se presenta un
 * cliente, le saluda con una frase "Hola cliente N". Este servidor sólo
 * atenderá hasta tres clientes, y desp ués finalizará su ejecución. Tras
 * atender cuatro clientes, el servidor deja de ofrecer su servicio.
 * 
 * @author Alex
 * @version 16/01/2018
 */

public class ServidorEj2 {

	public static void main(String[] args) {
		try {
			// Creación del socket servidor
			ServerSocket serverSocket = new ServerSocket();
			// Realización de la conexion
			InetSocketAddress iSA = new InetSocketAddress("localhost", 5000);
			serverSocket.bind(iSA);
			// Espera a que llegue una petición de socket
			Socket socket = serverSocket.accept();
			System.out.println("Se ha establecido la conexión");

			InputStream is = socket.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			OutputStream os = socket.getOutputStream();
			String mensaje = "Hola Cliente " + br.readLine();
			os.write(mensaje.getBytes());

//			 while (linea != null) {
//			 System.out.println("Recibiendo: " + linea);
//			 linea = br.readLine();
//			 }

			// Cerrando el socket de comunicación
			socket.close();
			// Cerrando el socket servidor
			serverSocket.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

}
