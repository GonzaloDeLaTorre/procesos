package ej2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ClienteEj2 {

	public static void main(String[] args) {
		try {
			Socket clientSocket = new Socket();
			InetSocketAddress addr = new InetSocketAddress("localhost", 5000);
			clientSocket.connect(addr);
			OutputStream os = clientSocket.getOutputStream();
			int cnt = 1;
			String mensaje = cnt + "\n";
			os.write(mensaje.getBytes());
			cnt++;

			InputStream is = clientSocket.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			System.out.println(br.readLine());

			// Cerrando el socket cliente
			clientSocket.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

}
