package ej1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorEj1 {

	public static void main(String[] args) {
		try {
			// Creación del socket servidor
			ServerSocket serverSocket = new ServerSocket();
			// Realización de la conexion
			InetSocketAddress iSA = new InetSocketAddress("localhost", 5000);
			serverSocket.bind(iSA);
			// Espera a que llegue una petición de socket
			Socket socket = serverSocket.accept();
			System.out.println("Se ha establecido la conexión");
			InputStream is = socket.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String linea = br.readLine();
			while (linea != null) {
				System.out.println("Recibiendo: " + linea);
				linea = br.readLine();
			}
			// Cerrando el socket de comunicación
			socket.close();
			// Cerrando el socket servidor
			serverSocket.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

}
