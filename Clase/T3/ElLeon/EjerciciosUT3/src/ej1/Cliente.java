package ej1;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		try {
			Socket clientSocket = new Socket();
			InetSocketAddress addr = new InetSocketAddress("localhost", 5000);
			clientSocket.connect(addr);
			OutputStream os = clientSocket.getOutputStream();
//			PrintWriter pw = new PrintWriter(os);

			boolean salir = true;
			do {
				System.out.print("Introduce cadena: ");
				Scanner sc = new Scanner(System.in);
				String mensaje = sc.next() + "\n";
				if (mensaje.equalsIgnoreCase("fin\n")) {
					salir = false;
				}
//				pw.println(mensaje.getBytes());
				os.write(mensaje.getBytes());
			} while (salir);
			clientSocket.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

}
