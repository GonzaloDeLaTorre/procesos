package ej1;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * Escribe un programa con una conexión entre Cliente y Servidor (sockets
 * stream) con las siguientes características:
 * 
 * • El cliente envía el texto tecleado en su entrada estándar al servidor
 * escribiendo en el socket (utiliza el puerto 5000).
 * 
 * • El servidor lee del socket y devuelve de nuevo al cliente el texto recibido
 * escribiendo en el socket. • El cliente lee del socket lo que le envía el
 * servidor de vuelta y lo muestra en pantalla.
 * 
 * • El programa servidor finaliza cuando el cliente termine la entrada por
 * teclado.
 * 
 * • El cliente finaliza cuando se detiene la entrada de datos con CTRL+C o
 * CTRL+Z.
 * 
 * @author Alex
 * @version 14/01/2018
 */
public class ClienteEj1 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		try {
			Socket clientSocket = new Socket();
			InetSocketAddress addr = new InetSocketAddress("localhost", 5000);
			clientSocket.connect(addr);
			OutputStream os = clientSocket.getOutputStream();

			boolean salir = true;
			do {
				System.out.print("Introduce cadena: ");
				Scanner sc = new Scanner(System.in);
				String mensaje = sc.next() + "\n";
				if (mensaje.equalsIgnoreCase("fin\n")) {
					salir = false;
				}
				os.write(mensaje.getBytes());
			} while (salir);
			clientSocket.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

}
