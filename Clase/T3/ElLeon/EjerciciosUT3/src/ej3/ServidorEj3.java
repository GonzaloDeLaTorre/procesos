package ej3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public class ServidorEj3 {

	public static void main(String[] args) {
		try {
			// Realización de la conexion
			InetSocketAddress addr = new InetSocketAddress("localhost", 5000);
			DatagramSocket datagramSocket = new DatagramSocket(addr);
			System.out.println("Se ha establecido la conexión");

			int longitud = 12;
			byte[] mensaje = new byte[longitud];
			DatagramPacket datagramPacket = new DatagramPacket(mensaje, longitud);
			datagramSocket.receive(datagramPacket);

			while (mensaje != null) {
				System.out.println("Recibiendo: " + new String(mensaje));
			}
			// Cerrando el socket de comunicación
			datagramSocket.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
