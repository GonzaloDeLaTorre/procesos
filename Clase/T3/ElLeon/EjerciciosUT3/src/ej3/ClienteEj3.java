package ej3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

/**
 * Escribe un programa con una conexión entre Cliente y Servidor (sockets
 * datagram) con las siguientes características:
 * 
 * • El cliente envía el texto tecleado en su entrada estándar al servidor
 * escribiendo en el socket (utiliza el puerto 5000).
 * 
 * • El servidor lee del socket y devuelve de nuevo al cliente el texto recibido
 * escribiendo en el socket. • El cliente lee del socket lo que le envía el
 * servidor de vuelta y lo muestra en pantalla.
 * 
 * • El programa servidor finaliza cuando el cliente termine la entrada por
 * teclado.
 * 
 * @author Alex
 * @version 21/01/2018
 */
public class ClienteEj3 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		try {
			DatagramSocket datagramSocket = new DatagramSocket();
			InetAddress addr = InetAddress.getByName("localhost");
			DatagramPacket datagramPacket;

			boolean salir = true;
			do {
				System.out.print("Introduce cadena: ");
				Scanner sc = new Scanner(System.in);
				String mensaje = sc.next() + "\n";
				if (mensaje.equalsIgnoreCase("fin\n")) {
					salir = false;
				}
				datagramPacket = new DatagramPacket(mensaje.getBytes(), mensaje.getBytes().length, addr, 5000);
				datagramSocket.send(datagramPacket);
			} while (salir);
			datagramSocket.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

}
