package salaDeChat;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
	static final int PUERTO = 1313;

	public static void main(String[] arg) {
		try {
			ServerSocket skServidor = new ServerSocket(PUERTO);
			System.out.println("Escucho el puerto " + PUERTO);

			for (int numCli = 1; numCli <= 5; numCli++) {
				Socket skCliente = skServidor.accept();
				System.out.println("Sirvo al cliente " + numCli);
				OutputStream aux = skCliente.getOutputStream();
				DataOutputStream flujo = new DataOutputStream(aux);
				
				InputStream entrada = skCliente.getInputStream();
				DataInputStream flujoEntrada = new DataInputStream(entrada);
				BufferedReader br = new BufferedReader(new InputStreamReader(flujoEntrada));
				String entrada2 = br.readLine();
				System.out.println("Hola cliente " + entrada2);

				flujo.writeUTF(entrada + "");
				skCliente.close();
			}
			System.out.println("Demasiados clientes por hoy");
			skServidor.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
