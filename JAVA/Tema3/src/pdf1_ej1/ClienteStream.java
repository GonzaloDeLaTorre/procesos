package pdf1_ej1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.omg.CORBA.CTX_RESTRICT_SCOPE;

import utilidades.Entrada;



public class ClienteStream {
    public static void main(String[] args) throws UnknownHostException, IOException {  // CTRL + D para abortar la ejecución, genera excepción y finaliza la ejecución.
    
    	String host = "localhost";
		int nPuerto = 5000; // puerto remoto
		Socket cliente = null;

		String entrada;
		int contador = 1;

		while (true) {
			System.out.println("Introduce cadena " + contador + ":");
			
			cliente = new Socket(host, nPuerto); // throws IOException &
													// UnkNownHostExcept
			// creamos flujo de salida al servidor
			DataOutputStream flujoSalida = new DataOutputStream(
					cliente.getOutputStream()); // throws IOException
			// el cliente envía un mensaje

			entrada = Entrada.cadena();

			//flujoSalida.writeUTF(entrada); trampilla
			
			PrintWriter printWriter = new PrintWriter (flujoSalida,true);
			//AQUÍ USO EL TRUE PARA EVITAR USAR EL FLUSH, PERO ACTUA DE MANERA SIMILAR
		    printWriter.println (entrada);
		    
			
			// throws IOException
			// creamos flujo de entrada al servidor
			DataInputStream flujoEntrada = new DataInputStream(cliente.getInputStream()); // throws IOException
			// envío datos del servidor
			System.out.println("Recibiendo datos del SERVIDOR....");
			System.out.println(flujoEntrada.readUTF()); // throws IOException
			
			// cerramos streams y sockets
			flujoEntrada.close(); // throws IOException
			flujoSalida.close(); // throws IOException
			printWriter.close ();
			
			contador++;
		}
    	
    }
}
