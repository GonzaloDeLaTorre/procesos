package pdf2_ej1_solucion;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Cliente {

	public static void main(String[] args) throws UnknownHostException{
		String host = "localhost";
		int nPuerto = 5000; //puerto remoto
		String variableString;
		
		System.out.println("********************");
		System.out.println("**    CLIENTE   **");
		System.out.println("********************");
		System.out.println("");
		System.out.println("");
		System.out.println("Introduce cadena: ");
		
		try{
			while(true){
				Socket cliente = new Socket (host, nPuerto); //throws IOException & UnkNownHostExcept
				
				//FLUJO ENTRADA DEL SERVIDOR
				DataInputStream flujoEntrada = new DataInputStream(cliente.getInputStream());
				System.out.println(flujoEntrada.readUTF());
				
				System.out.print("Nombre usuario: ");
				Scanner entrada=new Scanner(System.in);
				
				
				//FLUJO MENSAJE AL SERVIDOR
				DataOutputStream flujoSalida = new DataOutputStream(cliente.getOutputStream());
				flujoSalida.writeUTF(entrada.next());
			}
		}catch(IOException e){
			System.out.println("_________________________");
			System.out.println("*****Fin de conexi�n*****");
		}

	}

}