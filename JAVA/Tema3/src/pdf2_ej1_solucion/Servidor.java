package pdf2_ej1_solucion;
import java.io.*;
import java.net.*;
import java.util.Random;

public class Servidor {

	public static void main(String[] args) throws IOException {
		int puerto = 5000; //puerto
		ServerSocket servidor = new ServerSocket(puerto);
		String [] usuarios = new String[4];
		int [] claves = new int[4];
		int [] puertos = new int[4];
		Random claveAleatoria = new Random(); //numero aleatorio
		
		System.out.println("*******************");
		System.out.println("*    SERVIDOR     *");
		System.out.println("*******************");
		System.out.println("Esperando clientes....");
		
		try{
		//Cliente0
		while(true){
		Socket cliente0 = servidor.accept();
			int clienteConectado0 = cliente0.getPort();
			System.out.println("Cliente conectado: "+clienteConectado0);
			
			OutputStream avisar = cliente0.getOutputStream();
			DataOutputStream flujoSalida = new DataOutputStream(avisar);
			flujoSalida.writeUTF("Eres el cliente N� "+clienteConectado0+". Por favor indique un nombre de usuario... \n"+"SI INDICA 'FIN' EL PROGRAMA TERMINAR�.");
			puertos[0] = clienteConectado0; //almacenar puerto de usuario
			
			
			//ESCUCHAR NOMBRE
			InputStream entrada0 = cliente0.getInputStream();
			DataInputStream flujoEntrada = new DataInputStream(entrada0);
			String nombreUsuario0 = flujoEntrada.readUTF(); //almacenar nombre
			usuarios[0] = nombreUsuario0;
			System.out.print("Nombre: "+nombreUsuario0);
			
			//ENVIAR CLAVE
			OutputStream eClave = cliente0.getOutputStream();
			DataOutputStream flujoClave = new DataOutputStream(eClave);
			int aleatorio = (int)(claveAleatoria.nextDouble() * 9999 + 0);
			String aleatorioCadena = aleatorio+"";
			flujoClave.writeUTF(aleatorioCadena);
			claves[0]= aleatorio; //almacenar clave
			System.out.println(" Clave: "+claves[0]);
			
			cliente0.close(); //con cada conexi�n cerramos el sockets para iniciar con otro cliente
			//cliente0.close();
			if(nombreUsuario0.equalsIgnoreCase("fin")){
				servidor.close();
				System.out.println("*****************************");
				System.out.println("**     CONEXI�N CERRADA    **");
				System.out.println("*****************************");
			}
		}
		}catch(IOException e){
			System.out.println("______________________");
			System.out.println("*****Fin de conexi�n*****");
		}
	}
}
