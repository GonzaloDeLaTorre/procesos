package solucionEjercicio5;

public class PruebaJoin {
	public static void main (String[] args) throws InterruptedException {
		for(int i=1;i<=5;i++) {
			System.out.println("Creando hilo "+i);
			HiloJoin h=new HiloJoin(i);
			h.start();
			h.join();
		}
	}
}
