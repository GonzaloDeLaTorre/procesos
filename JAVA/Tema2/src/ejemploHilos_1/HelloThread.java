package ejemploHilos_1;

class HelloThread implements Runnable {
	Thread t;

	HelloThread() {
		t = new Thread(this, "Nuevo Thread");
		System.out.println("1.- Creado hilo: " + t);
		t.start(); // Arranca el nuevo hilo de ejecución. Ejecuta run
	}

	public void run() {
		System.out.println("2.- Hola desde el hilo creado!");
		System.out.println("3.- Hilo finalizando.");
	}
}