package pdf3_Ej4;

class Cola{
	class Nodo {
		int cliente;
		Nodo sig;
	}
	Nodo raiz,fondo;
	Caja cajas[];
	int N;
	public Cola(int N) {
		raiz=null;
		fondo=null;
		this.N = N;
		cajas = new Caja[N];
		for (int i= 0; i < N; i++){
			cajas[i]= new Caja();
		}
	}
	private boolean vacia (){
		if (raiz == null)
			return true;
		else
			return false;
	}
	private int cajaLibre(){
		int i = 0;
		while (i < N) {
			if (!cajas[i].ocupada()){
				break;
			}
			i++;
		}
		return i;
	}
	synchronized public int esperar (int id_cliente) throws InterruptedException
	{
		int caja_id;
		Nodo nuevo;
		nuevo = new Nodo ();
		nuevo.cliente = id_cliente;
		nuevo.sig = null;
		if (vacia ()) {
			raiz = nuevo;
			fondo = nuevo;
		} else {
			fondo.sig = nuevo;
			fondo = nuevo;
		}
		// Esperar hasta el turno
		while (((caja_id = cajaLibre()) == N) || (raiz.cliente != id_cliente) ){
			// Me bloqueo hasta que sea mi turno
			wait();
		}
		//Salgo de la cola
		raiz = raiz.sig;
		return caja_id;
	}
	public void atender(int id_caja, int pago) throws InterruptedException{
		cajas[id_caja].atender(pago);
	}
	synchronized public void finalizar_compra() throws InterruptedException {
		notify();
	}
}