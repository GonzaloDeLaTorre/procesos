package pdf3_Ej4;

public class ModernSuperMarket {
	public static void main(String[] args) throws InterruptedException {
		int N = Integer.parseInt (args[0]);
		Cola cola = new Cola(N);
		int M = Integer.parseInt (args[1]);
		Cliente clientes[] = new Cliente[M];
		for (int i= 0; i < M; i++){
			clientes[i]= new Cliente(i,cola);
			clientes[i].start();
		}
		try {
			for (int i= 0; i < M; i++){
				clientes[i].join();
			}
		} catch (InterruptedException ex) {
			System.out.println("Hilo principal interrumpido.");
		}
		System.out.println("Supermercado cerrado.");
		System.out.println("Ganancias: " + Resultados.ganancias);
		System.out.println("Tiempo medio de espera: " +	(Resultados.tiempo_espera /Resultados.clientes_atendidos));
	}
}