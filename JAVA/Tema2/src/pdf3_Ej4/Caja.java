package pdf3_Ej4;

import java.util.Random;

class Caja {
	private static final int MAX_TIME = 1000;
	private boolean ocupada;
	public Caja() {
		this.ocupada = false;
	}
	public boolean ocupada(){
		return ocupada;
	}
	synchronized public void atender(int pago) throws InterruptedException {
		ocupada = true;
		int tiempo_atencion = new Random().nextInt(MAX_TIME);
		Thread.sleep(tiempo_atencion);
		Resultados.ganancias += pago;
		Resultados.clientes_atendidos++;
		ocupada = false;
	}
}