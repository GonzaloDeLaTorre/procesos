package pdf3_Ej4;

import java.util.Random;

class Cliente extends Thread {
	private static final int MAX_DELAY = 2000;
	private static final int MAX_COST = 100;
	private int id;
	private Cola cola;
	Cliente (int id, Cola cola) {
		this.id = id;
		this.cola = cola;
	}
	public void run() {
		try {
			int numero_caja;
			System.out.println("Cliente " + id + "	realizando compra");
			Thread.sleep(new Random().nextInt(MAX_DELAY));
			long s = System.currentTimeMillis();
			numero_caja = cola.esperar(id);
			cola.atender(numero_caja, new Random().nextInt(MAX_COST));
			System.out.println("Cliente " + id + " atendido	en caja " + numero_caja);
			cola.finalizar_compra();
			System.out.println("Cliente " + id + "	finalizando");
			long espera = System.currentTimeMillis() - s;
			Resultados.tiempo_espera += espera;
			System.out.println("Cliente " + id + " saliendo	después de esperar " + espera);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}