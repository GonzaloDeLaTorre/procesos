package pdf2;

import java.util.concurrent.Semaphore;

public class Ejercicio1 implements Runnable {


	static int n = 0; //contador de incrementos y decrementos
	final int N = 15; //cantidad de veces que cada proceso va a incrementar/decrementar
	final static int M = 13; //cantidad de procesos incrementador/decrementador respectivamente
	int id;

	public Ejercicio1(int id){
		this.id=id;
	}

	public void run() {
		if(id==1){
			this.incrementar();
		}else{
			this.decrementar();
		}
	}
	public void decrementar(){
		for (int i=0; i<N;i++){
			n=n-1;
		}
	}

	public void incrementar(){
		for (int i=0; i<N;i++){
			n=n+1;	
		}
	}

	public static void main(String[] args) throws InterruptedException {

		Thread incre [] = new Thread [M]; //se crea un array para poder almacenar los incrementadores
		Thread decre [] = new Thread [M]; //se crea un array para poder almacenar los decrementadores
		for (int i=0; i<M; i++){  
			incre [i]= new Thread (new Ejercicio1(1)); //se llena el array de los incrementadores
			incre [i].start(); //se lanza cada uno de los threads creados desde el array.
			decre [i]= new Thread (new Ejercicio1(2)); //se llena el array de los decrementadores
			decre [i].start();
		}
		for (int i=0; i<M; i++){//se hace el join a cada uno de los procesos que hay en el array id
			incre [i].join();        //Se hace en un bucle a parte porque si se hace en el bucle de arriba
			decre [i].join();
		}
		System.out.println("El valor final de la variable n es " + n);

	}
}