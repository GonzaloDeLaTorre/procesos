package estudio;

public class Repartidor extends Thread {

	private int numeroFolletos;
	private int repartidor;

	public Repartidor(int numeroFolletos, int repartidor) {
		super();
		this.numeroFolletos = numeroFolletos;
		this.repartidor = repartidor;
	}

	public void run() {
		try {
			long tiempoInicial = System.currentTimeMillis();
			Thread.sleep(numeroFolletos * 2000);
			long tiempoFinal = (System.currentTimeMillis() - tiempoInicial) / 1000;
			System.out.println("El Repartidor " + repartidor + ". Reparte " + numeroFolletos
					+ " folletos que tarda en repartilos " + tiempoFinal + " segundos.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
