package estudio;

import java.util.Random;

//import utilidades.Entrada;

public class Empresa {

	public static void main(String[] args) {
		System.out.print("Numero de repartidores que trabajan hoy: ");
		int numeroRepatidores = 5;

		for (int i = 1; i <= numeroRepatidores; i++) {
			Repartidor repartidor = new Repartidor(new Random().nextInt(10), i);
			repartidor.start();
		}

	}

}