package estudio;

public class VariosHilos {

	public static void main(String[] args) {
		
		for (int i = 0; i < 5; i++) {
			HiloC h1 = new HiloC(i+1);
			h1.start();
		}
		
		System.out.println("Todos los hilos creados.");
	}

}
