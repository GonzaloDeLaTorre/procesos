package estudio;

public class PruebaJoin {

	public static void main(String[] args) throws InterruptedException {

		System.out.println("Creando Hilo 1");
			HiloJoin h1 = new HiloJoin(1);
			Thread hilo = new Thread(h1);
			hilo.start();
			hilo.join();
		
		System.out.println("Creando Hilo 2");
			HiloJoin h2 = new HiloJoin(2);
			Thread hilo2 = new Thread(h2);
			hilo2.start();
			hilo2.join();
		
		System.out.println("Todos los hilos creados.");
		
	}

}
