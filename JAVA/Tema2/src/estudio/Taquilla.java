package estudio;

public class Taquilla {

	public static void main(String[] args) {
		//Creamos un teatro. Las variables est�n setteadas por defecto.
		Teatro teatro = new Teatro(); 
		
		//Creamos los clientes y los arrancamos:			
		Espectador uno= new Espectador(1,teatro,(int)(Math.random()*(30-45+1)+45));
		uno.start();
		Espectador dos= new Espectador(2,teatro,(int)(Math.random()*(30-45+1)+45));
		dos.start();
		
		//hacemos los joins para que se ejecuten antes que el main y no se muestren los resultados antes de hayan acabado los hilos de comprar entradas.
				try {
					uno.join();
					dos.join();				
				} catch (InterruptedException e) {					
					e.printStackTrace();
				}
			
			System.out.println("Los clientes de Madrid han comprado "+teatro.entradasVendidasMadrid+" entradas.");
			System.out.println("Los clientes de fuera de Madrid han comprado "+teatro.entradasVendidasNoMadrid+" entradas.");
			System.out.println("Quedan por vender en taquilla "+teatro.localidades+" entradas.");
		

	}

}
