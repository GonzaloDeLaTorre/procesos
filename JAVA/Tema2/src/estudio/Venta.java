package estudio;

import java.util.Random;

/*
 * SE VAN A VENDER 1240 DE FORMA ALEATORIA, HAY QUE CALCULAR CUANTAS SE VENDEN EN MADRID, EN EL RESTO DE PROVINCIAS Y LAS ENTRADAS QUE SOBRAN.
 * */

public class Venta {

	public static void main(String[] args) throws InterruptedException {
		
		int cnt1=0;
		int cnt2=0;
		int cnt3=0;
		
//		while (1240-(cnt1+cnt2) >= 45) {
			ClienteMadrid cm = new ClienteMadrid((int) Math.floor(Math.random()*(30-45+1)+45));
//			cnt1=cnt1+(int) Math.floor(Math.random()*(30-45+1)+45);
			cm.start();
//			cm.join();
//			synchronized (cm) {};
			
//			if ((1240-(cnt1+cnt2)) <= 30) {break;}
			
			ClienteResto cr = new ClienteResto((int) Math.floor(Math.random()*(30-45+1)+45));
//			cnt2=cnt2+(int) Math.floor(Math.random()*(30-45+1)+45);
			cr.start();
//			cr.join();
//			synchronized (cr) {};
//		}
		
		cnt3=1240-(cnt1+cnt2);
		
		System.out.println("En la provincia de Madrid se han vendido "+cnt1+" entradas.");
		System.out.println("En el resto de provincias se han vendido "+cnt2+" entradas.");
		System.out.println("Han quedado: "+cnt3+" entradas sin vender.");

	}

}
