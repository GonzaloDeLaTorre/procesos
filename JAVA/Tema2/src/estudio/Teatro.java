package estudio;

public class Teatro {
		//Atributos:	
		public int localidades=1240;
		public int entradasVendidasMadrid=0;
		public int entradasVendidasNoMadrid=0;
		//Constructor:
		public Teatro() {
				super();
		}
		
		//M�todo de compra de entradas que usa el hilo Espectador: al sincronizar el m�todo entero, s�lo puede acceder a hacer la compra un s�lo hilo.
		public  void comprarEntradas(boolean esMadrile�o, int entradasAComprar) {		
			//System.out.println( entradasAComprar+" "+ localidades);			
			if(localidades>=entradasAComprar) {//Hay suficientes entradas para comprar
				localidades=localidades-entradasAComprar;
				if(esMadrile�o) {
					entradasVendidasMadrid+=entradasAComprar;
				}else {
					entradasVendidasNoMadrid+=entradasAComprar;
				}			
			}	
		}
		
//		@Override
//		public String toString() {
//			return "Teatro [entradasVendidasMadrid=" + entradasVendidasMadrid + ", entradasVendidasNoMadrid="
//									+ entradasVendidasNoMadrid + "localidades=" +localidades+"]";
//		}
	
	
}
