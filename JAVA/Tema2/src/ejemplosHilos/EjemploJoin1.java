package ejemplosHilos;

public class EjemploJoin1 {
	public static void main (String[] args) throws InterruptedException {
		Thread h1 = new Thread(new NuevoEjemploJoin1 ( "1" ));
		Thread h2 = new Thread(new NuevoEjemploJoin1 ( "2" ));
		Thread h3 = new Thread(new NuevoEjemploJoin1 ( "3" ));
		h1. start ();
		h1. join (); // Así aseguro que el hilo 1 se ejecute siempre el primero.
		
		h2. start ();
		h3. start ();
		
		
		h2. join ();
		h3. join ();
	}

}
