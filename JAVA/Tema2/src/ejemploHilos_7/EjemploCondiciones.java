package ejemploHilos_7;

public class EjemploCondiciones extends javax.swing.JFrame {
    CondicionesHiloContador hc = null;
    /**
     * Creates new form EjemploCondiciones
     */
    public EjemploCondiciones() {
        initComponents();
        this.setSize(500, 300);
        hc = new CondicionesHiloContador(this.jsContador);
        this.setTitle("PSP_ejempl9_condiciones");
    }



    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jsContador = new javax.swing.JSlider();
        jbArrancar = new javax.swing.JButton();
        jbContinuar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new java.awt.GridLayout(3, 1));

        jsContador.setMaximum(100000);
        jsContador.setValue(0);
        jPanel1.add(jsContador);

        jbArrancar.setText("Arrancar");
        jbArrancar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbArrancarActionPerformed(evt);
            }
        });
        jPanel1.add(jbArrancar);

        jbContinuar.setText("Continuar");
        jbContinuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbContinuarActionPerformed(evt);
            }
        });
        jPanel1.add(jbContinuar);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbArrancarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbArrancarActionPerformed
        // TODO add your handling code here:
        hc.start();
    }//GEN-LAST:event_jbArrancarActionPerformed

    private void jbContinuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbContinuarActionPerformed
        // TODO add your handling code here:
        hc.desbloquea();
    }//GEN-LAST:event_jbContinuarActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EjemploCondiciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EjemploCondiciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EjemploCondiciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EjemploCondiciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EjemploCondiciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton jbArrancar;
    private javax.swing.JButton jbContinuar;
    private javax.swing.JSlider jsContador;
    // End of variables declaration//GEN-END:variables
}
