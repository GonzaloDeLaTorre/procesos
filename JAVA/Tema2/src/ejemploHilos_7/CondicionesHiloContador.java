package ejemploHilos_7;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JSlider;

public class CondicionesHiloContador extends Thread {
    JSlider js;

    public CondicionesHiloContador(JSlider _js) {
        this.js = _js;
    }

    @Override
    public void run() {
        while (js.getValue()<js.getMaximum()){
            cuenta();
        }
    }

    /**
     * En el momento en el que el value del slider llegue a 3000, hace un wait.
     * La condici�n ha de estar en un while (no en un if), para evitar que
     * ante una modificaci�n por parte de otro hilo del valor del slider se 
     * pudiese volver a tener 3000 como valor al salir de la espera.
     * 
     * En este ejemplo ese caso se da si el usuario no modifica el slider, pero
     * ilustra la problem�tica.
     */
    synchronized void cuenta() {
        js.setValue(js.getValue()+1);
        //if (js.getValue()==3000) {//Si usamos if, podemos tener problemas
        while (js.getValue()==3000) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        /*
            Aqu� ir�a el c�digo que no se puede ejecutar si js.getValue es 
            igual a 3000:
        */
        if (js.getValue()==3000) {
            System.out.println ("ERROR FATAL");
        }
    }
    synchronized void desbloquea(){
        notify();
    }
}