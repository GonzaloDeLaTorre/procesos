package ejemploHilos_8;

import java.util.Queue;

class Consumidor extends Thread {
	 private Queue queue;
	 private int maxSize;

	 public Consumidor(Queue queue, int maxSize, String name) {
	  super(name);
	  this.queue = queue;
	  this.maxSize = maxSize;
	 }

	 @Override
	 public void run() {
	  while (true) {
	   synchronized (queue) {
	    while (queue.isEmpty()) {
	     System.out.println("La cola esta vacia," + "El hilo consumidor esta esperando que el hilo productor que ponga algo en la cola");
	     try {
	      queue.wait();
	     } catch (Exception ex) {
	      ex.printStackTrace();
	     }
	    }
	    System.out.println("Consumiendo valor : " + queue.remove());
	    queue.notifyAll();
	   }
	  }
	 }
	}