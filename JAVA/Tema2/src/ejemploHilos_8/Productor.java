package ejemploHilos_8;

import java.util.Queue;
import java.util.Random;

class Productor extends Thread {
	 private Queue queue;
	 private int maxSize;

	 public Productor(Queue queue, int maxSize, String name) {
	  super(name);
	  this.queue = queue;
	  this.maxSize = maxSize;
	 }

	 @Override
	 public void run() {
	  while (true) {
	   synchronized (queue) {
	    while (queue.size() == maxSize) {
	     try {
	      System.out.println("La cola esta llena, el hilo productor esta esperando que el consumidor tome algo de la cola");
	      queue.wait();
	     } catch (Exception ex) {
	      ex.printStackTrace();
	     }
	    }
	    Random random = new Random();
	    int i = random.nextInt();
	    System.out.println("Produciendo valor : " + i);
	    queue.add(i);
	    queue.notifyAll();
	   }
	  }
	 }
	}