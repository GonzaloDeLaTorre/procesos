package ejemploHilos_8;

import java.util.LinkedList;
import java.util.Queue;

public class ProductorConsumidorEnJava {
	
	 public static void main(String args[]) {
		 
		  System.out.println("Como usar el metodo wait y notify en Java");
		  System.out.println("Resolviendo el problema del Productor-Consumidor");
		  Queue buffer = new LinkedList<>();
		  int maxSize = 10;
		  Thread producer = new Productor(buffer, maxSize, "PRODUCER");
		  Thread consumer = new Consumidor(buffer, maxSize, "CONSUMER");
		  producer.start();
		  consumer.start();
		  
	 }
	 
}