package pdf4_Ej2;

public class Hilo_Conector extends Thread {

	private int idHilo;

	public Hilo_Conector (int _idHilo){
	        this.idHilo =_idHilo;
	    }
	

	public int getIdHilo() {
		return idHilo;
	}

	public void setIdHilo(int idHilo) {
		this.idHilo = idHilo;
	}


	public void run() {
		for (int i = 1; i <= 100; i++) {
			if (getIdHilo()==1) {
				System.out.println("Hilo " + getIdHilo() + ": "+ i);
				System.out.println("	    Hilo 2: " + (101-i));
			}
		}
	}

}
