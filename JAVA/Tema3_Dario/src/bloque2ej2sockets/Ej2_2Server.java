package bloque2ej2sockets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Ej2_2Server {

	public static void main(String[] args) {
		
		try {
			ServerSocket serverSocket = new ServerSocket();
			serverSocket.bind(new InetSocketAddress("localhost",5000));
			System.out.println("Esperando Cliente...");
	        Socket socket = serverSocket.accept();
	        
	        InputStream is = socket.getInputStream();
	        byte[] mensaje = new byte[100];
			is.read(mensaje);
			
			
			String pregunta = new String(mensaje),preguntaDep="",respuesta;
			for (int i = 0; i < pregunta.length(); i++) {
				if (pregunta.charAt(i)=='?') {
					preguntaDep+=pregunta.charAt(i);
					break;
				} else 
					preguntaDep+=pregunta.charAt(i);
			}
			
			System.out.println("Pregunta recibida: "+preguntaDep);
			
	        
	        switch (preguntaDep) {
			case "¿Cómo te llamas?":
				respuesta = "Me llamo Ejercicio 2";
				break;
			case "¿Cuántas lineas de código tienes?":
				respuesta=""+numLineasCodigo();
				break;			
			default:
				respuesta = "No entiendo la pregunta";
				break;
			}
	        System.out.println(respuesta);
        } catch (IOException e) {
		e.printStackTrace();
		}
		
	}
	
	//método para sacar lineas de código
	public static int numLineasCodigo() {
		int ress = 0;
		try {
			FileReader fr = new FileReader(new File("socketss/bloque2ej2sockets/Ej2_2Server.java"));
			LineNumberReader lnr= new LineNumberReader(fr);
			lnr.skip(Long.MAX_VALUE);		//salta la línea leida completamente(Long.MAX_VALUE) y contaviliza las lineas saltadas
			ress = lnr.getLineNumber();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ress;
	}
}
