package bloque2ej2sockets;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import utilidades.Entrada;

public class Ej2_2Cliente {

	public static void main(String[] args) {
		
		try {
			Socket clientSocket = new Socket();
			clientSocket.connect(new InetSocketAddress("localhost",5000));
			
			OutputStream os = clientSocket.getOutputStream();
			
			System.out.println("Introduzca una pregunta para el Servidor: ");
			String nombre=Entrada.cadena();
	        os.write(nombre.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
