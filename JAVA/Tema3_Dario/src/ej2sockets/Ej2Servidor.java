package ej2sockets;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Ej2Servidor {

	public static void main(String[] args) {
		
		try {
			
	        
			String frase;
			
			for (int i = 0; i < 4; i++) {
				ServerSocket serverSocket = new ServerSocket();
				InetSocketAddress iSA = new InetSocketAddress("localhost",5000);		//Realización del bind
				serverSocket.bind(iSA);			//Espera a que llegue una petición de socket
		        System.out.println("Esperando Cliente...");
		        Socket socket = serverSocket.accept();		//Se ha establecido la conexión
		        frase = "Hola Cliente "+(i+1);
		        System.out.println("Mensaje enviado a Cliente "+(i+1));
		        OutputStream os = socket.getOutputStream();
		        os.write(frase.getBytes());
		        
		        socket.close();
		        serverSocket.close();
			}
			System.out.println("Finalizado Servidor");
            
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        

	}

}
