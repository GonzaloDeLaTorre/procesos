package ej2sockets;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Ej2Clientes {

	public static void main(String[] args) {
		
		try {
			Socket clientSocket = new Socket();			//Creando socket cliente
	        InetSocketAddress addr = new InetSocketAddress("localhost",5000);		//Estableciendo conexión
			clientSocket.connect(addr);
			
			InputStream is = clientSocket.getInputStream();
			
	        byte[] mensaje = new byte[14];
			is.read(mensaje);
			String frase = new String(mensaje);
			System.out.println(frase);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

}
