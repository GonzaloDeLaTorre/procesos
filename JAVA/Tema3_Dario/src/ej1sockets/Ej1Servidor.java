package ej1sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Ej1Servidor {

	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket();
	        InetSocketAddress iSA = new InetSocketAddress("localhost",5000);		//Realización del bind
	        serverSocket.bind(iSA);			//Espera a que llegue una petición de socket
	        System.out.println("Esperando Cliente...");
	        Socket socket = serverSocket.accept();		//Se ha establecido la conexión
	        System.out.println("Conexion establecida");
	        InputStream is = socket.getInputStream();
	        OutputStream os = socket.getOutputStream();
	        
	        String linea;
	        byte[] mensaje = new byte[100];
	        
	        do {
	        	is.read(mensaje);
		        linea= new String(mensaje);
		        System.out.println(linea);
	        	if (!linea.equals("fin")) {		//no funciona bien
	        		
	        		System.out.println("Mensaje:" + linea +" Recivido");
	        		os.write(linea.getBytes());
	        		System.out.println("Enviando " + linea);
	        		mensaje = new byte[14];
	        		
	        	}
			} while (!linea.equals("fin"));
	        
	        System.out.println("Finalizado Servidor");
            
	        socket.close();
            serverSocket.close();
            
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        

	}

}
