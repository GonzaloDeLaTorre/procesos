package ej1sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;

import utilidades.Entrada;

public class Ej1Cliente {		//sin terminar

	public static void main(String[] args) {
        try {
    		Socket clientSocket = new Socket();			//Creando socket cliente
            InetSocketAddress addr = new InetSocketAddress("localhost",5000);		//Estableciendo conexión
			clientSocket.connect(addr);
			
			InputStream is = clientSocket.getInputStream();
	        OutputStream os = clientSocket.getOutputStream();
	        String cadena;
	        
	        String linea2;
	        byte[] mensaje = new byte[14];
	        do {
				System.out.println("Introduce cadena:");
				cadena=Entrada.cadena();
	        	if (!cadena.toLowerCase().equals("fin")) {
	        		os.write(cadena.getBytes());
	        		System.out.println("Mensage "+cadena+" enviado");
	        		
	        		is.read(mensaje);
	        		linea2= new String(mensaje);
			        
			        System.out.println("Eco "+linea2);
	        	} else 
	        		os.write(cadena.getBytes());
			} while (!cadena.toLowerCase().equals("fin"));
	        
	        
	        
	        
	        
	        System.out.println("Finalizado Cliente");
	        clientSocket.close();
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        

	}

}
