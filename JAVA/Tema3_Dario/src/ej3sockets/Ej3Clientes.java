package ej3sockets;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import utilidades.Entrada;

public class Ej3Clientes {

	public static void main(String[] args) {	// hacer el ej1 con datagram
		try {
			DatagramSocket ds = new DatagramSocket();		//Crea el socket
			InetAddress addr = InetAddress.getByName("localhost");	//Crea la InetAddress
			int lon = 12;
			String mensaje;
			do {
				System.out.println("Introduzca un mensaje");
				mensaje = Entrada.cadena();
				DatagramPacket datagrama = new DatagramPacket(mensaje.getBytes(),mensaje.getBytes().length,addr,5555);//Crea el datagrama
				ds.send(datagrama);			//se envia el mensaje
				
				byte[] mensajeRec = new byte[lon];					//Array de bytes donde almacenar el mensaje
		        datagrama = new DatagramPacket(mensajeRec, lon);	//Se crea el datagrama
		        ds.receive(datagrama);		//se recive el mensaje
		        System.out.println("ECO "+ new String(mensajeRec));
				
			} while (!mensaje.equals("fin"));
			
	        System.out.println("Cliente finalizado");
	        ds.close();
	        
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		
		

	}

}
