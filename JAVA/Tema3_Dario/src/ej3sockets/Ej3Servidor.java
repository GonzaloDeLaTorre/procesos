package ej3sockets;

import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Ej3Servidor {

	public static void main(String[] args) {	// hacer el ej1 con datagram
		
		try {
			System.out.println("Esperando a cliente...");
			InetSocketAddress addr = new InetSocketAddress("localhost",5555);
			DatagramSocket ds = new DatagramSocket(addr);//Crea el socket
	        int longitud = 12;//Longitud del mensaje
	        byte[] mensaje = new byte[longitud];//Array de bytes donde almacenar el mensaje
	        String s;
	        do {
	        	DatagramPacket datagrama = new DatagramPacket(mensaje, longitud);//Se crea el datagrama
	        	ds.receive(datagrama);
		        s=new String (mensaje);
		        
	        	if (!s.equals("fin")) {
	        		System.out.println("Mensaje: "+s+" recibido");
			        
			        ds.send(datagrama);		//se utiliza el mismo datagrama para recivir mensajes como para enviarlos al cliente
			        System.out.println("Mensaje "+s+" reenviado");
				}
	        	mensaje = new byte[longitud];
			} while (!s.equals("fin"));
	        
	        System.out.println("Servidor Finalizado");
	        ds.close();
	        
	    } catch (SocketException e) {					//s.getBytes(),s.getBytes().length,5555,addr
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
