package ej4sockets;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Ej4Servidor {

	public static void main(String[] args) {
		
		try {
			ServerSocket serverSocket = new ServerSocket();
			serverSocket.bind(new InetSocketAddress("localhost",5000));
			Socket socket = serverSocket.accept();
			System.out.println("Conexion establecida");
	        InputStream is = socket.getInputStream();
	        
	        String linea;
	        byte[] mensaje = new byte[14];
	        
	        do {
	        	is.read(mensaje);
		        linea= new String(mensaje);
        		System.out.println("Mensaje:" + linea +" Recibido");
        		
        		mensaje = new byte[14];
			} while (!linea.equals("fin"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        

	}

}
