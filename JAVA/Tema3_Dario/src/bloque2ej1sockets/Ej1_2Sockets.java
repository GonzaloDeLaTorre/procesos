package bloque2ej1sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import utilidades.Entrada;

public class Ej1_2Sockets {

	public static void main(String[] args) {
		
		try {
			Socket clientSocket = new Socket();			//Creando socket cliente
			clientSocket.connect(new InetSocketAddress("localhost",5000));
			
			InputStream is = clientSocket.getInputStream();
	        OutputStream os = clientSocket.getOutputStream();
	        
	        byte[] mensaje = new byte[100];
	        is.read(mensaje);
	        String num=new String(mensaje);
	        
	        System.out.println("Soy Cliente nº "+num);
	        System.out.println("Introducir Nombre de usuario a enviar al Servidor:");
	        String nombre=Entrada.cadena();
	        os.write(nombre.getBytes());
	        
	        mensaje = new byte[100];
	        is.read(mensaje);
	        System.out.println("Mi contrase generada es: "+new String(mensaje));
	        System.out.println("Cliente "+num+" finalizado");
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
