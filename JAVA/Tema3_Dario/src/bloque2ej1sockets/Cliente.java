package bloque2ej1sockets;

public class Cliente {

	int numCliente;
	String nombreCliente;
	String passCliente;
	
	public Cliente(int numCliente, String nombreCliente, String passCliente) {
		super();
		this.numCliente = numCliente;
		this.nombreCliente = nombreCliente;
		this.passCliente = passCliente;
	}
	
	@Override
	public String toString() {
		return "Cliente nº: " + numCliente + ", Nombre: " + nombreCliente + ", Contraseña: " + passCliente;
	}

	public int getNumCliente() {
		return numCliente;
	}
	public void setNumCliente(int numCliente) {
		this.numCliente = numCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getPassCliente() {
		return passCliente;
	}
	public void setPassCliente(String passCliente) {
		this.passCliente = passCliente;
	}
	
}
