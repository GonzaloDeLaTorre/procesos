package bloque2ej1sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Ej1_2Server {

	public static void main(String[] args) {
		
		try {
			ServerSocket serverSocket = new ServerSocket();
			serverSocket.bind(new InetSocketAddress("localhost",5000));
	        
	        ArrayList<Cliente> arc = new ArrayList<Cliente>();
	        String nombreCliente = null;
	        
	        for (int i = 0; i < 5; i++) {
	        	
		        System.out.println("Esperando Cliente...");
		        Socket socket = serverSocket.accept();

		        InputStream is = socket.getInputStream();
		        OutputStream os = socket.getOutputStream();
		        
				
				os.write((""+(i+1)).getBytes());			//enviando número de Cliente
				System.out.println("Nº de Cliente: "+(i+1)+" - Enviado");
				byte[] mensaje = new byte[100];
				is.read(mensaje);							//nombre de usuario
				System.out.println("Nombre de Cliente: "+new String(mensaje));
				String pass = crearRandom(new Random().nextInt(10000));	//contraseña generada
	        	os.write(pass.getBytes());					//enviando contraseña
	        	System.out.println("Contraseña: "+pass+" - Envianda");
	        	System.out.println();
	        	
	        	arc.add(new Cliente(i+1,new String(mensaje),pass));
	        	
	        	socket.close();
			}
	        
	        for (int i = 0; i < arc.size(); i++) {		//bucle para enseñar los clientes
				System.out.println(arc.get(i));
			}
	        
	        
	        System.out.println("Servidor Finalizado");
            serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		
	}

	private static String crearRandom(int random) {
		String pass = ""+random;
		if (random<1000) {
			pass="0"+pass;
			if (random<100) {
				pass="0"+pass;
				if (random<10) {
					pass="0"+pass;
					return pass;
				}
			}
		} 
		return pass;
	}
}
