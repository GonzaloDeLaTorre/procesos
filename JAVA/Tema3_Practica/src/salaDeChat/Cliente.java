package salaDeChat;

import java.io.DataOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {

	static final String HOST = "localhost";
	static final int PUERTO = 1313;

	@SuppressWarnings("resource")
	public static void main(String[] arg) {
		try {
			System.out.println("Introduce un nombre de cliente:");
			Scanner sc = new Scanner(System.in);
			String nombreCliente = sc.next();

			Socket skCliente = new Socket(HOST, PUERTO);
			DataOutputStream flujo = new DataOutputStream(skCliente.getOutputStream());
			PrintWriter printWriter = new PrintWriter(flujo, true);
			printWriter.println(nombreCliente);

			flujo.close(); // throws IOException
			printWriter.close();
			skCliente.close();

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
