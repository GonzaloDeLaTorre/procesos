package ejercicio1;

public class Sumador {

	public static void main(String[] args) {
		int res=suma(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		System.out.println(res);
	}

	private static int suma(int n1, int n2) {
		int resultado=0;
		resultado=n1+n2;
		return resultado;
	}

}