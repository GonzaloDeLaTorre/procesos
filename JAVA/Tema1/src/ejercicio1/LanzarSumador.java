package ejercicio1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class LanzarSumador { //Poner en el argumento: java -jar rutaJavaRunnableExportadoSumador arg1 arg2

	public static void main(String[] args) throws IOException {
		Process process = new ProcessBuilder(args).start(); //Crear proceso hijo.
		
		InputStream is = process.getInputStream(); //is es un cojunto de bytes.
		//InputStream = Leer la salida del proceso hijo. Lo que el hijo le devuelve lo recogemos con el InputStream.
		
		InputStreamReader isr = new InputStreamReader(is); //Ya estoy trabajando con flujo de caracteres.
		
		BufferedReader br = new BufferedReader(isr);
		
		String line;
		
		System.out.println("Salida del proceso "+Arrays.toString(args)+":");
		System.out.println("El resultado es: ");
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
	}
}