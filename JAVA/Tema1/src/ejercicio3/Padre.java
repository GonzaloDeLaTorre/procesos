package ejercicio3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import utilidades.Entrada;

public class Padre {

	public static void main(String[] args) throws IOException { //Meter por argumento... java -jar rutaCompletaEjecutable
		try {
			String entrada="";
			
			while (entrada!=null) {

				Process process = new ProcessBuilder(args).start(); //Crear proceso hijo.
				
				InputStream is = process.getInputStream(); //is es un cojunto de bytes.
				//InputStream = Leer la salida del proceso hijo. Lo que el hijo le devuelve lo recogemos con el InputStream.
				
				InputStreamReader isr = new InputStreamReader(is); //Ya estoy trabajando con flujo de caracteres.
				
				BufferedReader br = new BufferedReader(isr);
				
				String line;
				line = br.readLine();
				
				System.out.println("Numero Aleatorio: "+line);
				br.close();
				
				System.out.print("Introduzca Enter para generar otro\no 'fin' para terminar: ");
				entrada=Entrada.cadena();
				if (entrada.equals("fin")) {
					entrada=null;
				}
			}
		} catch (Exception e) {
			System.err.println("FALLO");
		}
	}
}
