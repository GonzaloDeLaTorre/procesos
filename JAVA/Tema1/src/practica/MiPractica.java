package practica;

import java.io.File;
import java.io.IOException;

public class MiPractica {

	public static void main(String[] args) throws IOException {
		//  javac src/practica/MiPractica.java
		try {
			new ProcessBuilder(args).start();
	
			File ruta=new File(args[1]);
			if (!ruta.exists()) {
				System.err.println("El directorio o el fichero no existe.");
			}
				
			if (ruta.exists()) {
				System.out.println("Se ha realizado todo correctamente.");
			}
			
		} catch (Exception e) {
			System.err.println("No se ha compilado el archivo MiPractica.java");
		}

	}

}
