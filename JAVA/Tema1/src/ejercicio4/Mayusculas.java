package ejercicio4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;

import utilidades.Entrada;

public class Mayusculas {
	
	public static void main(String[] args) { //Meter por argumento... java -jar rutaCompletaEjecutable
		
		System.out.println("Introduzca palabra: ");
		String palabra = Entrada.cadena();
		
		try {
			//Crear proceso hijo.
			Process hijo = new ProcessBuilder(args).start();
			
			/*Enviar informacion: OutputStream*/
			/*Recoger informacion: InputStream*/
			
			//El proceso padre envia una palabra al hijo
			OutputStream os = hijo.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			PrintWriter pw = new PrintWriter(osw);
			pw.print(palabra);
			pw.close();
			
			//El proceso padre recoge la palabra del proceso hijo
			InputStream is = hijo.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line= br.readLine();
			
			//Imprimimos por pantalla
			System.out.println("Salida del proceso " + Arrays.toString(args) + ":");
			while (line != null) {
				System.out.println(line);
			}
			br.close();
			
		} catch (IOException e) {
			System.err.println("FALLO");
		}
		
	}
	
}
