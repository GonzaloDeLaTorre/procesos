package app1;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Random;

import utilidades.Entrada;
import utilidades.GestorBD;

public class Aplicacion1 {

	public static void main(String[] args) throws Exception {
		
		java.sql.Connection con = null;
		GestorBD g = new GestorBD();
		con = g.conectarBBDD();

		System.out.println("Número de registros a insertar: ");
		int registros = Entrada.entero();
		
		System.out.println("Número de hilos a insertar: ");
		int hilos = Entrada.entero();
		
		ArrayList<Hilos> al = new ArrayList<Hilos>();
		
		for (int i = 0; i < hilos; i++) {
			al.add(new Hilos());
		}
		
		int cntHilos = 1;
		
		while (registros > cntHilos || registros == cntHilos) {
			for (int j = 0; j < al.size(); j++) {
				
				al.get(j).run(); // CREAR HILOS (CREA REGISTROS)
	
				System.out.println("Hilo "+(j+1)+", Registro "+cntHilos);
				
				if (cntHilos == registros) {
					System.exit(0);
				}
				cntHilos++;
			}
		}
		
	}

}
