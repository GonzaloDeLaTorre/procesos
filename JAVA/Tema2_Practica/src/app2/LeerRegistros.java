package app2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.GestorBD;

public class LeerRegistros {

	public static void main(String[] args) throws Exception {
		long inicio = System.currentTimeMillis();
		
		java.sql.Connection con = null;
		GestorBD g = new GestorBD();
		con = g.conectarBBDD();
		
		leerRegistros(con);

	}

	private static void leerRegistros(Connection con) throws SQLException, InterruptedException {
		
		long inicio = System.nanoTime();
		
		java.sql.Connection conexion = con;
		String select = "SELECT * FROM T_INDIVIDUOS";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(select);
		ArrayList<Registros> al = new ArrayList<Registros>();
		int cnt = 0;
		while (rs.next()) {
			al.add(new Registros(rs.getInt(1), rs.getString(2), rs.getInt(3)));
			cnt = cnt + Integer.valueOf(rs.getString(3));
		}
		rs.close();
		st.close();
		
		System.out.println("\n	ID	|	EMAIL	|	INGRESOS\n");
		for (Registros i : al) {
			System.out.println(i);
//			Thread.sleep(1470);
		}
		
        long fin = System.nanoTime();  
        double tiempo = (double) ((fin - inicio)/100000000);
        System.out.println("\nSuma: "+ cnt +".");
        System.out.println("Tiempo empleado en la lectura y la suma: "+ tiempo/10 +" segundos.");
		
	}

}
