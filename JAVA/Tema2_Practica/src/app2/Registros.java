package app2;

public class Registros {

	int id;
	String email;
	int ingresos;
	
	public Registros(int id, String email, int ingresos) {
		super();
		this.id = id;
		this.email = email;
		this.ingresos = ingresos;
	}


	@Override
	public String toString() {
		return "	 " + id + "		 " + email + "		 " + ingresos;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getIngresos() {
		return ingresos;
	}


	public void setIngresos(int ingresos) {
		this.ingresos = ingresos;
	}
	
	
}
