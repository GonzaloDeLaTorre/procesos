package ej4;

public class Cuenta  extends Thread{

	private int saldo;
	private int numero_reintegros;
	
	public void run() {
		
	}

	public Cuenta(int saldo, int numero_reintegros) {
		super();
		this.saldo = saldo;
		this.numero_reintegros = numero_reintegros;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public int getNumero_reintegros() {
		return numero_reintegros;
	}

	public void setNumero_reintegros(int numero_reintegros) {
		this.numero_reintegros = numero_reintegros;
	}

}
