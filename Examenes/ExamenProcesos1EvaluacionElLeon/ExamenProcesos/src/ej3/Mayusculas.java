package ej3;

public class Mayusculas extends Thread {

	private String cadena;

	public Mayusculas(String cadena) {
		super();
		this.cadena = cadena;
	}

	public void run() {
		System.out.println("HILO MAYÚSCULAS DICE: " + cadena.toUpperCase());
	}

}
