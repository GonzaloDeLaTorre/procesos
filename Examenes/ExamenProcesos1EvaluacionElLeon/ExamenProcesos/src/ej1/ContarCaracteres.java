package ej1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ContarCaracteres {

	public static void main(String[] args) {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);
			String cadena = "";
			while ((cadena = br.readLine()) != null) {
				System.out.println("La cadena " + cadena + " tiene " + cadena.length() + " caracteres.");
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}