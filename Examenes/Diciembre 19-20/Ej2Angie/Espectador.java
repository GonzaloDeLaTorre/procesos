package teatro;
import java.util.Random;
public class Espectador extends Thread{

	boolean esMadrileņo;
	int idComprador;
	Teatro t;
	int entradasAComprar;
	
			public Espectador(int idComprador, Teatro t, int entradasAComprar) {
				super();		
				this.idComprador = idComprador;
				this.t = t;
				this.entradasAComprar = entradasAComprar;
				//Calculamos el boolean:
				Random r = new Random(); 
				esMadrileņo= r.nextBoolean(); //Aleatorio si es de madrid o no
			}
	
	@Override
	public String toString() {
		return "El hilo " + idComprador+" ha comprado "+ entradasAComprar + " para "+esMadrileņo+". Quedan en taquilla "+t.localidades;
	}

					public void run() {						
						while(true) {
							
								if(t.localidades>=entradasAComprar) {
								  t.comprarEntradas(esMadrileņo,entradasAComprar);
								}else {
									break;
								}	
								
								try {
									Thread.sleep(10);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}	
						}					
					}	
		
		
	
	
	}
