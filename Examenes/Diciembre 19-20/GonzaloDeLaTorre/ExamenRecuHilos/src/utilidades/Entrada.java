package utilidades;

import java.io.*;

public class Entrada {
	static String inicializar() {
		String buzon = "";
		InputStreamReader flujo = new InputStreamReader(System.in);
		BufferedReader teclado = new BufferedReader(flujo);
		try {
			buzon = teclado.readLine();
		} catch (Exception e) {
			System.out.append("Entrada incorrecta)");
		}
		return buzon;
	}

	public static int entero() {
		int valor = Integer.parseInt(inicializar());
		return valor;
	}

	public static double real() {
		double valor = Double.parseDouble(inicializar());
		return valor;
	}

	public static String cadena() {
		String valor = inicializar();
		return valor;
	}

	public static char caracter() {
		String valor = inicializar();
		return valor.charAt(0);
	}
}

/*
Creación de hilos:
	- Dos clases mínimo, el main y la claseHilo.
	- En el main creamos el objeto claseHilo y para ejecutarlo utilizamos: X.start();
		en la clase claseHilo creamos el metodo public void run(){} y aplicamos el extends de Thread para que se pueda hacer en el main los start();,etc.
		
	Método join():
		- Igual que al crear un hilo normal pero en vez de extends de Thread es implements Runnable en la clase claseHilo;
		- En la clase main justo después de hacer el X.start() hacemos X.join(); y nos aseguramos de que ese hilo se ejecute en ese mismo momento.
		- Si da error hay que hacer puente entre el objeto creado y el start del mismo con Thread hilo=new Thread(h1); siendo h1 el objeto creado y el start será con hilo.
*/