package ej1;

public class Capacidad extends Thread {
	
	int capacidad = 135;

	public Capacidad(int capacidad) {
		super();
		this.capacidad = capacidad;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
	
//	public void run() {
//		
//	}
}
