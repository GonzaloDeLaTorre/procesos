package ej2;

import java.util.Random;

import utilidades.Entrada;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		
		/*
		 * Ejercicio resuelto con 10 entradas totales.
		 * */
		System.out.print("Número de veces que repondrá los artículos el proveedor: ");
		int nVeces = Entrada.entero();
		
		int cantidadReal = 10;

		int cnt = 0;
		while (nVeces > cnt) {
			for (int i = 0; i < 10; i++) {
				Random r = new Random();
				int n = r.nextInt(2)+1;
				
				cantidadReal = cantidadReal - n;				
				//Condicion de compra del Cliente:
				//	si compra más entradas de las que se pueden vender se sale para que el proveedor pueda reponerlas,
				//	si no, se efectua la compra.
				if (cantidadReal < 3) {	
					//Condiciones para restablecer el número de entradas disponibles
					if (cantidadReal == 1) {
						cantidadReal = cantidadReal + 2;
					}
					if (cantidadReal == 2) {
						cantidadReal = cantidadReal + 1;
					}
					break;
				} else {
					Cliente c = new Cliente(n);			
					c.run();
				}
			}
			Proveedor p = new Proveedor(cantidadReal);
			p.run();
			//Restablecemos el número de entradas totales disponibles.
			cantidadReal = p.vasijasRespuestas;
			cnt++;
		}	
	}

}
