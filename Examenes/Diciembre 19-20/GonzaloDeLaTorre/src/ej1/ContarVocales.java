package ej1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ContarVocales {

	public static void main(String[] args) {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);
			String cadena = br.readLine();
			int cnt = 0;
			for (int i = 0; i < cadena.length(); i++) {
				if (cadena.charAt(i)=='a' || cadena.charAt(i)=='e' || cadena.charAt(i)=='i' || cadena.charAt(i)=='o' || cadena.charAt(i)=='u') {
					cnt++;
				}	
			}
			System.out.println("La cadena '" + cadena + "' tiene " + cnt + " vocales.");
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}