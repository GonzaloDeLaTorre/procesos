package ej3;

public class Archivero extends Thread {

	private int numeroLibros;

	public Archivero(int numeroLibros) {
		super();
		this.numeroLibros = numeroLibros;
	}

	public int getNumeroLibros() {
		return numeroLibros;
	}

	public void setNumeroLibros(int numeroLibros) {
		this.numeroLibros = numeroLibros;
	}
	
	public void run() {
		System.out.println("El archivador registra " + numeroLibros + " libros.");
	}

}
