package ej3;

public class Registrador extends Thread {

	private int numeroLibros;

	public Registrador(int numeroLibros) {
		super();
		this.numeroLibros = numeroLibros;
	}

	public int getNumeroLibros() {
		return numeroLibros;
	}

	public void setNumeroLibros(int numeroLibros) {
		this.numeroLibros = numeroLibros;
	}

	public void run() {
		System.out.println("El registrador registra " + numeroLibros + " libros.");
	}

}
