package ej3;

import utilidades.Entrada;

public class Biblioteca {

	public static void main(String[] args) {
		
		System.out.print("Numero de libros: ");
		int numeroLibros = Entrada.entero(); //Mayor que 8
		
		try {
			Mesa.registrarLibro(numeroLibros);
			
			Thread.sleep(2000);
			
			Mesa.archivarLibro(numeroLibros);
			
			Thread.sleep(2000);
			
			if (numeroLibros-Mesa.mesa > 0) {
				Mesa.registrarLibro(numeroLibros-Mesa.mesa);
				
				Thread.sleep(2000);
				
				Mesa.archivarLibro(numeroLibros-Mesa.mesa);
			}

			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	
		
	}

}
