package ej3;

public class Mesa {
	
	static int mesa=8;
	
	public static void registrarLibro(int param) {
		if (param >= mesa) {
			for (int i = 1; i <= mesa; i++) {
				Registrador registro = new Registrador(i);
				registro.start();
			}
		} else {
			for (int i = 1; i <= (param+mesa)-mesa; i++) {
				Registrador registro = new Registrador(i);
				registro.start();
			}
		}
	}
	
	public static void archivarLibro(int param) {
		if (param >= mesa) {
			for (int i = 1; i <= mesa; i++) {
				Archivero ar = new Archivero(i);
				ar.start();
			}
		} else {
			for (int i = 1; i <= (param+mesa)-mesa; i++) {
				Archivero ar = new Archivero(i);
				ar.start();
			}
		}
	}

}
