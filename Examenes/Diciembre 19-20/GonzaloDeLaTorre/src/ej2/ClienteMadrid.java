package ej2;

public class ClienteMadrid {
	
	private int cantidad;

	public ClienteMadrid(int cantidad) {
		super();
		this.cantidad = cantidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

}
