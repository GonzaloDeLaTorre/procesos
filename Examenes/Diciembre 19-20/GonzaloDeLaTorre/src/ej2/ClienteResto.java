package ej2;

public class ClienteResto {
	
	private int cantidad;
	
	public ClienteResto(int cantidad) {
		super();
		this.cantidad = cantidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

}
