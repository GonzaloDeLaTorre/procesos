package ej2;

public class Proveedor implements Runnable {
	
	int vasijasRespuestas;

	public Proveedor(int vasijasRespuestas) {
		super();
		this.vasijasRespuestas = vasijasRespuestas;
	}

	@Override
	public void run() {
		vasijasRespuestas = vasijasRespuestas + 7;
		
		System.out.println("El proveedor ha repuesto las vasijas.");
	}

}
