package ej2;

public class Cliente implements Runnable {

	int vasijasCompradas;

	public Cliente(int vasijasCompradas) {
		super();
		this.vasijasCompradas = vasijasCompradas;
	}

	@Override
	public void run() {
		
		System.out.println("El Cliente ha comprado " + vasijasCompradas + " vasijas.");
	}
		
	
}
